#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import sys
import rosbag
import numpy as np
import sensor_msgs.point_cloud2 as pc2
import geometry_msgs.msg._TwistStamped as gms


def read():

    file_name = sys.argv[1].split('.')[0]
    path = sys.argv[1]
   
    radar_txt = open("/home/alex-stend/datasets/new_xsens_datasets/bags/" + file_name + "_radar.txt", "w")
    xsens_txt = open("/home/alex-stend/datasets/new_xsens_datasets/bags/" + file_name + "_xsens.txt", "w")
    # f_pc = open("/home/user/datasets/smartmicro_datasets/bags/pc_" + file_name + ".txt", "w")

    radar_msg_cnt = 0
    radar_pc_arr = []
    xsens_pc_arr = []
    xsens_msg_cnt = 0

    for topic, msg, t in rosbag.Bag(path).read_messages():
        # print topic
        if topic == "/points":
            radar_pc = pc2.read_points(msg, skip_nans=True,
                                       field_names=("x", "y", "z", "v", "ampl"))
            for p in pc2.read_points(msg, field_names=("x", "y", "z", "v", "ampl"), 
                                     skip_nans=True):
                #  print (" x : %f  y: %f  z: %f" % (p[0], p[1], p[2]))
                 radar_pc_arr.append(p[0:5])
            np.save("/home/alex-stend/datasets/new_xsens_datasets/bags/"+ "radar/"
                    + str(radar_msg_cnt).rjust(8, '0'), radar_pc_arr)
            radar_pc_arr = []
            radar_msg_cnt += 1
            radar_txt.write(str(t) + "\n")

        if topic == "/velocity":

            xsens_gms_x = msg.twist.linear.x
            xsens_pc_arr.append(xsens_gms_x)

            xsens_gms_y = msg.twist.linear.y
            xsens_pc_arr.append(xsens_gms_y)
            
            xsens_gms_z = msg.twist.linear.x
            xsens_pc_arr.append(xsens_gms_z)
            
            np.save("/home/alex-stend/datasets/new_xsens_datasets/bags/"+ "xsens/"
                + str(xsens_msg_cnt).rjust(8, '0'), xsens_pc_arr)            
            xsens_pc_arr = []
            xsens_msg_cnt += 1
            xsens_txt.write(str(t) + "\n")


    radar_txt.close()
    xsens_txt.close()

read()
