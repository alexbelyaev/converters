#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import sys
import rosbag
import numpy as np
# import sensor_msgs.point_cloud2 as pc2


def read():

    file_name = sys.argv[1].split('.')[0]
    path = "/home/alex-stend/datasets/raw_data_17_02_2022/" + file_name + ".bag"
   
    f_radar_bin = open("/home/alex-stend/datasets/raw_data_17_02_2022/" + file_name + ".bin", "wb")
    f_radar_txt = open("/home/alex-stend/datasets/raw_data_17_02_2022/" + file_name + ".txt", "w")

    radar_byte_arr = np.array([], dtype=bytes)

    for topic, msg, t in rosbag.Bag(path).read_messages():
        # print topic
        if topic == "/mrl_raw":
            pass
            radar_byte_arr = np.asarray(msg.raw_data, dtype=bytes)
            f_radar_bin.write(radar_byte_arr)
            f_radar_txt.write(str(t) + "\n")      

    f_radar_bin.close()
    f_radar_txt.close()

read()
