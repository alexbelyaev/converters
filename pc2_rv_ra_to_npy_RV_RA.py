#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import sys
import rosbag
import numpy as np
import sensor_msgs.point_cloud2 as pc2


def read():

    file_name = sys.argv[1].split('.')[0]
    print(file_name)
    path = "/home/stand/datasets/raw_rv_ra_data_tests/" + file_name + ".bag"
    print(path.split('.')[0])
    f_radar_pc2_txt_ra = open("/home/stand/datasets/raw_rv_ra_data_tests" + "/radar_pc2/ra_pc/" + file_name + ".txt", "w")
    f_radar_pc2_txt_rv = open("/home/stand/datasets/raw_rv_ra_data_tests" + "/radar_pc2/rv_pc/" + file_name + ".txt", "w")
    print("/home/stand/datasets/raw_rv_ra_data_tests" + "/radar_pc2/" + file_name + ".txt")
    
    radar_ra_msg_cnt = 0
    radar_rv_msg_cnt = 0
    radar_pc2_arr = []

    radar_byte_arr = np.array([], dtype=bytes)

    for topic, msg, t in rosbag.Bag(path.split('.')[0] + ".bag").read_messages():

        if topic == "/radar/front/ra_points":
            pass
            radar_pc2 = pc2.read_points(msg, skip_nans=True,
                                        field_names=("x", "y", "z", "v", "ampl"))
            for p in pc2.read_points(msg, field_names=("x", "y", "z", "v", "ampl"),
                                     skip_nans=True):
                 #print (" x : %f  y: %f  z: %f" % (p[0], p[1], p[2]))
                 radar_pc2_arr.append(p[0:5])
            np.save("/home/stand/datasets/raw_rv_ra_data_tests" + "/radar_pc2/ra_pc/" + "ra_pc_"
                    + str(radar_ra_msg_cnt).rjust(8, '0'), radar_pc2_arr)   
            radar_pc2_arr = []
            radar_ra_msg_cnt += 1
            f_radar_pc2_txt_ra.write(str(t) + "\n")
        if topic == "/radar/front/rv_points":
            pass
            radar_pc2 = pc2.read_points(msg, skip_nans=True,
                                        field_names=("x", "y", "z", "v", "ampl"))
            for p in pc2.read_points(msg, field_names=("x", "y", "z", "v", "ampl"),
                                     skip_nans=True):
                 #print (" x : %f  y: %f  z: %f" % (p[0], p[1], p[2]))
                 radar_pc2_arr.append(p[0:5])
            np.save("/home/stand/datasets/raw_rv_ra_data_tests" + "/radar_pc2/rv_pc/" + "rv_pc_"
                    + str(radar_rv_msg_cnt).rjust(8, '0'), radar_pc2_arr)   
            radar_pc2_arr = []
            radar_rv_msg_cnt += 1
            f_radar_pc2_txt_rv.write(str(t) + "\n")

    f_radar_pc2_txt_ra.close()
    f_radar_pc2_txt_rv.close()

read()
