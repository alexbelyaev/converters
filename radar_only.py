#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 18:57:26 2024

@author: radar
"""

#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import os
import sys
from tqdm import tqdm
import numpy as np
import rosbag
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2

bridge = CvBridge()

def create_directories(bag_name):
  file_name = bag_name.split('.')[0]

  if not os.path.exists(file_name):
    print('created ' + file_name + ' folder')
    os.mkdir(file_name)

  if not os.path.exists(file_name + '/radar/'):
    print('created ' + file_name + '/radar/ folder')
    os.mkdir(file_name + '/radar/')

def one_bag_processing(bag_name):
  file_name = bag_name.split('.')[0]

  f_radar_bin = open(file_name + "/radar/" + file_name + ".bin", "wb")
  f_radar_txt = open(file_name + "/radar/radar_ts.txt", "w")

  radar_byte_arr = np.array([], dtype=bytes)

  for topic, msg, t in tqdm(rosbag.Bag(bag_name).read_messages()):
    if topic == "/dvm_raw_radar_data":
      radar_byte_arr = np.asarray(msg.raw_data, dtype=bytes)
      f_radar_bin.write(radar_byte_arr)
      f_radar_txt.write(str(t) + "\n")

  f_radar_bin.close()
  f_radar_txt.close()


def read_bags():
  for idx, arg in enumerate(sys.argv):
    if idx > 0:
      print('\n' + arg + ' processing..')
      create_directories(arg)
      one_bag_processing(arg)


if __name__ == '__main__':
  read_bags()
