#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import os
import sys
from tqdm import tqdm
import numpy as np
import rosbag
import sensor_msgs.point_cloud2 as pc2


def create_directories(bag_name):
  file_name = bag_name.split('.')[0]

  if not os.path.exists(file_name):
    print('created ' + file_name + ' folder')
    os.mkdir(file_name)

  if not os.path.exists(file_name + '/lidar/'):
    print('created ' + file_name + '/lidar/ folder')
    os.mkdir(file_name + '/lidar/')


def one_bag_processing(bag_name):
  file_name = bag_name.split('.')[0]

  f_lidar_txt = open(file_name + "/lidar/lidar_ts.txt", "w")

  lidar_msg_cnt = 1
  lidar_pc_arr = []

  for topic, msg, t in tqdm(rosbag.Bag(bag_name).read_messages()):

    if topic == "/hdl32/velodyne_points":
      for p in pc2.read_points(msg, field_names=("x", "y", "z"),
                               skip_nans=True):
          #  print (" x : %f  y: %f  z: %f" % (p[0], p[1], p[2]))
        lidar_pc_arr.append(p[0:3])
      np.save(file_name + "/lidar/"
              + str(lidar_msg_cnt).rjust(8, '0'), lidar_pc_arr)
      lidar_pc_arr = []
      lidar_msg_cnt += 1
      f_lidar_txt.write(str(t) + "\n")

  f_lidar_txt.close()


def read_bags():
  for idx, arg in enumerate(sys.argv):
    if idx > 0:
      print('\n' + arg + ' processing..')
      create_directories(arg)
      one_bag_processing(arg)


if __name__ == '__main__':
  read_bags()
