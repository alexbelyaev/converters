#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import sys
import rosbag
import numpy as np
import sensor_msgs.point_cloud2 as pc2


def read():

    file_name = sys.argv[1]
    #file_name = sys.argv[1].split('.')[0]
    path = "/home/alex-stend/radar_bags/"
    f_radar_bin = open(path + "bags/radar/" + file_name.split('.')[0] + ".bin", "wb")
    f_radar_txt = open(path + "bags/radar/" + file_name.split('.')[0] + ".txt", "w")
    f_lidar_txt = open(path + "bags/lidar/" + file_name.split('.')[0] + ".txt", "w")
    
    lidar_msg_cnt = 0
    lidar_pc_arr = []

    radar_byte_arr = np.array([], dtype=bytes)
    print(path + file_name)

    for topic, msg, t in rosbag.Bag(path + file_name).read_messages():
        # print topic
        if topic == "/mrl_raw":
            pass
            radar_byte_arr = np.asarray(msg.raw_data, dtype=bytes)
            f_radar_bin.write(radar_byte_arr)
            f_radar_txt.write(str(t) + "\n")
            
            # f_pc.write("\n" + str(msg.header.seq) + "\n")

        if topic == "/cloud":
            pass
            lidar_pc = pc2.read_points(msg, skip_nans=True,
                                       field_names=("x", "y", "z"))
            for p in pc2.read_points(msg, field_names=("x", "y", "z"),
                                     skip_nans=True):
                 #print (" x : %f  y: %f  z: %f" % (p[0], p[1], p[2]))
                 lidar_pc_arr.append(p[0:3])
            np.save(path + "bags/lidar/"
                    + str(lidar_msg_cnt).rjust(8, '0'), lidar_pc_arr)            
            lidar_pc_arr = []
            lidar_msg_cnt += 1
            f_lidar_txt.write(str(t) + "\n")
#        if topic == "/mrl_pc":
#            pass
#            
#            f_pc.write("\n" + str(msg.points[:20]) + "\n")            

    f_radar_bin.close()
    f_radar_txt.close()
    f_lidar_txt.close()
    f_lidar_txt.close()

read()
