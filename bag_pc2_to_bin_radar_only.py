#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import sys
import rosbag
import numpy as np
import sensor_msgs.point_cloud2 as pc2


def read():

    file_name = sys.argv[1].split('.')[0]
    path = sys.argv[1]
   
    f_radar_txt = open("/home/alex-stend/datasets/smartmicro_datasets/bags/" + file_name + ".txt", "w")
    # f_radar_txt = open(path + "bags/radar/" + file_name.split('.')[0] + ".txt", "w")
    # f_pc = open("/home/user/datasets/smartmicro_datasets/bags/pc_" + file_name + ".txt", "w")

    radar_msg_cnt = 0
    radar_pc_arr = []

    for topic, msg, t in rosbag.Bag(path).read_messages():
        # print topic
        if topic == "/radar/center/points":
            radar_pc = pc2.read_points(msg, skip_nans=True,
                                       field_names=("x", "y", "z", "v", "ampl"))
            for p in pc2.read_points(msg, field_names=("x", "y", "z", "v", "ampl"), 
                                     skip_nans=True):
                 #print (" x : %f  y: %f  z: %f" % (p[0], p[1], p[2]))
                 radar_pc_arr.append(p[0:5])
            np.save("/home/alex-stend/datasets/smartmicro_datasets/bags/"+ "radar/"
                    + str(radar_msg_cnt).rjust(8, '0'), radar_pc_arr)
            radar_pc_arr = []
            radar_msg_cnt += 1
            f_radar_txt.write(str(t) + "\n")

    f_radar_txt.close()

read()
