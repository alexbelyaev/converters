#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import sys
import rosbag
import numpy as np
import sensor_msgs.point_cloud2 as pc2


def read():

    file_name = sys.argv[1].split('.')[0]
    print(file_name)
    path = "/home/user/radar_bags/corner/corners_08_07_2021_32_pulses/" + file_name + ".bag"
    print(path.split('.')[0])
    f_radar_pc2_txt = open("/home/user/radar_bags/corner/corners_08_07_2021_32_pulses/" + "/radar_pc2/" + file_name + ".txt", "w")
    print("/home/user/radar_bags/corner/corners_08_07_2021_32_pulses" + "/radar_pc2/" + file_name + ".txt")
    
    radar_msg_cnt = 0
    radar_pc2_arr = []

    radar_byte_arr = np.array([], dtype=bytes)

    for topic, msg, t in rosbag.Bag(path.split('.')[0] + ".bag").read_messages():

        if topic == "/mrl_pc":
            pass
            radar_pc2 = pc2.read_points(msg, skip_nans=True,
                                        field_names=("x", "y", "z", "v", "ampl"))
            for p in pc2.read_points(msg, field_names=("x", "y", "z", "v", "ampl"),
                                     skip_nans=True):
                 #print (" x : %f  y: %f  z: %f" % (p[0], p[1], p[2]))
                 radar_pc2_arr.append(p[0:5])
            np.save("/home/user/radar_bags/corner/corners_08_07_2021_32_pulses/" + "/radar_pc2/"
                    + str(radar_msg_cnt).rjust(8, '0'), radar_pc2_arr)   
            radar_pc2_arr = []
            radar_msg_cnt += 1
            f_radar_pc2_txt.write(str(t) + "\n")

    f_radar_pc2_txt.close()

read()
