#!/usr/bin/python
"""
Created on Tue Mar 10 14:26:16 2020

@author: user
"""

import os
import sys
from tqdm import tqdm
import numpy as np
import rosbag
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2

bridge = CvBridge()

def create_directories(bag_name):
  file_name = bag_name.split('.')[0]

  if not os.path.exists(file_name):
    print('created ' + file_name + ' folder')
    os.mkdir(file_name)

  if not os.path.exists(file_name + '/rawCam/'):
    print('created ' + file_name + '/rawCam/ folder')
    os.mkdir(file_name + '/rawCam/')


def one_bag_processing(bag_name):
  file_name = bag_name.split('.')[0]

  f_usbCam_txt = open(file_name + "/rawCam/rawCam_ts.txt", "w")
  cam_msg_cnt = 0

  for topic, msg, t in tqdm(rosbag.Bag(bag_name).read_messages()):

    if topic == "/stereo/left/image_raw":
      cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
      cv2.imwrite(file_name + '/rawCam/camera_image_' + str(cam_msg_cnt).rjust(8, '0') + '.jpeg', cv2_img)
      f_usbCam_txt.write(str(t) + "\n")
      cam_msg_cnt += 1

  f_usbCam_txt.close()


def read_bags():
  for idx, arg in enumerate(sys.argv):
    if idx > 0:
      print('\n' + arg + ' processing..')
      create_directories(arg)
      one_bag_processing(arg)


if __name__ == '__main__':
  read_bags()
